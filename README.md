# Give me cows!

This docker container is a simple cowsay to make the lives better for those unfortunate individuals with broken package managers...

It is a direct implementation on alpine of cowsay in nodejs by Fabio Crisci <fabio.crisci@gmail.com>.
- https://github.com/piuccio/cowsay
- https://www.npmjs.com/package/cowsay

# How to use

Basic use:
    
    docker run -it --rm registry.gitlab.com/chrisweeksnz/cowsay -h
    
Consider using an alias in your shell (bash in this example):

    alias cowsay='docker run -it --rm registry.gitlab.com/chrisweeksnz/cowsay'
