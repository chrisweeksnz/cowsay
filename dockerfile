FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      project-site="https://www.npmjs.com/package/cowsay" \
      container-site="https://gitlab.com/chrisweeksnz/cowsay"
RUN apk update && \
    apk add nodejs-npm && \
    npm install -g cowsay
ENTRYPOINT ["/usr/bin/cowsay"]